package siammod.service;

public class LehmerRandom {
    private int a, m;
    private double r0, rC;

    public LehmerRandom(int a, int m, double r0) {
        this.a = a;
        this.m = m;
        this.r0 = r0;
        this.rC = r0;
    }

    public double nextDouble() {
        double r = a * r0 % m;
        rC = a * r0 / m;
        r0 = r;
        return rC - Math.floor(rC);
    }
}
