package siammod.service.distgen;

import siammod.service.LehmerProducer;
import siammod.service.SequenceGenerator;

import java.util.ArrayList;
import java.util.List;

public class ExponentialGenerator implements SequenceGenerator {
    private List<Double> lehmerSequence;
    private Double lambda;

    public ExponentialGenerator(int lehmerRateA, int lehmerRateB, int rateR, Double lambda) {
        LehmerProducer lp = new LehmerProducer(lehmerRateA, lehmerRateB, rateR);
        lehmerSequence = lp.provideSequence();
        this.lambda = lambda;
    }

    @Override
    public List<Double> provideSequence() {
        List<Double> result = new ArrayList<>();
        for (double tmp:lehmerSequence)
        {
            result.add(Math.abs(Math.log(tmp)/lambda));
        }
        return result;
    }
}
