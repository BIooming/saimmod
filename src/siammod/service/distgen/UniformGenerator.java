package siammod.service.distgen;

import siammod.service.LehmerProducer;
import siammod.service.SequenceGenerator;

import java.util.ArrayList;
import java.util.List;

public class UniformGenerator implements SequenceGenerator {
    private List<Double> lehmerSequence;
    private Double rateA;
    private Double rateB;

    public UniformGenerator(int lehmerRateA, int lehmerRateB, int rateR,
                            Double rateA, Double rateB) {
        LehmerProducer lp = new LehmerProducer(lehmerRateA, lehmerRateB, rateR);
        lehmerSequence = lp.provideSequence();
        this.rateA = rateA;
        this.rateB = rateB;
    }

    public List<Double> provideSequence() {
        List<Double> result = new ArrayList<>();
        for (Double value : lehmerSequence) {
            result.add(rateA + (rateB - rateA) * value);
        }
        return result;
    }
}
