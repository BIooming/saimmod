package siammod.service.distgen;

import siammod.service.LehmerProducer;
import siammod.service.SequenceGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GammaGenerator implements SequenceGenerator {
    private List<Double> lehmerSequence;
    private Double eta;
    private Double lamdba;

    public GammaGenerator(int lehmerRateA, int lehmerRateB, int rateR,
                          Double eta, Double lambda) {
        LehmerProducer lp = new LehmerProducer(lehmerRateA, lehmerRateB, rateR);
        lehmerSequence = lp.provideSequence();
        this.eta = eta;
        this.lamdba = lambda;
    }

    public List<Double> provideSequence() {
        Random random = new Random(lehmerSequence.size() - 1);
        List<Double> result = new ArrayList<>();
        for (int i = 0; i <= lehmerSequence.size() - 1; i++) {
            double r = 1;
            for (int j = 0; j <= Math.floor(eta); j++) {
                r *= lehmerSequence.get(random.nextInt());
            }
            result.add(-Math.abs(Math.log(r) / lamdba));
        }
        return result;
    }
}
