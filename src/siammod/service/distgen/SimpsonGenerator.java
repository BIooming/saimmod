package siammod.service.distgen;

import siammod.service.LehmerProducer;
import siammod.service.SequenceGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SimpsonGenerator implements SequenceGenerator {
    private List<Double> lehmerSequence;
    private List<Double> simpsonSequence;

    public SimpsonGenerator(int lehmerRateA, int lehmerRateB, int rateR,
                            Double rateA, Double rateB) {
        LehmerProducer lp = new LehmerProducer(lehmerRateA, lehmerRateB, rateR);
        lehmerSequence = lp.provideSequence();
        simpsonSequence = new ArrayList<>();
        for (double val : lehmerSequence) {
            simpsonSequence.add(rateA / 2 + (rateB / 2 - rateA / 2) * val);
        }
    }

    public List<Double> provideSequence() {
        Random random = new Random(lehmerSequence.size() - 1);
        List<Double> result = new ArrayList<>();
        for (int i = 0; i <= lehmerSequence.size()-1; i++) {
            double r1 = simpsonSequence.get(random.nextInt());
            double r2 = simpsonSequence.get(random.nextInt());
            result.add(r1 + r2);
        }
        return result;
    }
}
