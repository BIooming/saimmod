package siammod.service.distgen;

import siammod.service.LehmerProducer;
import siammod.service.SequenceGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TriangularGenerator implements SequenceGenerator {
    private List<Double> lehmerSequence;
    private Double rateA;
    private Double rateB;

    public TriangularGenerator(int lehmerRateA, int lehmerRateB, int rateR,
                               Double rateA, Double rateB) {
        LehmerProducer lp = new LehmerProducer(lehmerRateA, lehmerRateB, rateR);
        lehmerSequence = lp.provideSequence();
        this.rateA = rateA;
        this.rateB = rateB;
    }

    public List<Double> provideSequence() {
        Random random = new Random(lehmerSequence.size() - 1);
        List<Double> result = new ArrayList<>();
        for (int i = 0; i <= lehmerSequence.size()-1; i++) {
            double r1 = lehmerSequence.get(random.nextInt());
            double r2 = lehmerSequence.get(random.nextInt());
            result.add(rateA + (rateB - rateA) * Math.max(r1, r2));
        }
        return result;
    }
}
