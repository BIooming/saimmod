package siammod.service.distgen;

import siammod.service.LehmerProducer;
import siammod.service.SequenceGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GaussGenerator implements SequenceGenerator {
    private List<Double> lehmerSequence;
    private Double expc;
    private Double mdev;

    public GaussGenerator(int lehmerRateA, int lehmerRateB, int rateR,
                          Double expc, Double mdev) {
        LehmerProducer lp = new LehmerProducer(lehmerRateA, lehmerRateB, rateR);
        lehmerSequence = lp.provideSequence();
        this.expc = expc;
        this.mdev = mdev;
    }

    public List<Double> provideSequence() {
        Random random = new Random(lehmerSequence.size() - 1);
        List<Double> result = new ArrayList<>();
        for (int i = 0; i <= lehmerSequence.size() - 1; i++) {
            double r = 0;
            for (int j = 0; j <= 5; j++) {
                r += lehmerSequence.get(random.nextInt());
            }
            result.add(expc + mdev * Math.sqrt(2) * (r - 3));
        }
        return result;
    }
}
