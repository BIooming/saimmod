package siammod.service;

import java.util.ArrayList;
import java.util.List;

public class LehmerProducer implements SequenceGenerator {
    private static List<Double> randomsq;
    int lehmerRateA;
    int lehmerRateB;
    int rateR;

    public LehmerProducer(int a, int m, int r) {
        lehmerRateA = a;
        lehmerRateB = m;
        rateR = r;
    }


    @Override
    public List<Double> provideSequence() {
        double tmp;
        randomsq = new ArrayList<>();
        LehmerRandom lr = new LehmerRandom(lehmerRateA, lehmerRateB, rateR);
        randomsq.add(lr.nextDouble());
        tmp = lr.nextDouble();
        while (!randomsq.contains(tmp)) {
            randomsq.add(tmp);
            tmp = lr.nextDouble();
        }
        //randomsq.Add(tmp);
        return randomsq;
    }
}
