package siammod.service;

public enum DistributionMode {
    UNIFORM,
    EXPONENTIAL,
    GAUSS,
    GAMMA,
    TRIANGULAR
}
