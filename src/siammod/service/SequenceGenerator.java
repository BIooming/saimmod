package siammod.service;

import java.util.List;

public interface SequenceGenerator  {

    List<Double> provideSequence();
}
