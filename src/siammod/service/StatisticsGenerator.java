package siammod.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StatisticsGenerator {
    private int period = 0;
    private double mx, Dx, sigmax, beg, end, interv;
    private String pathname = "rs.csv";
    private List<Double> valueSequence;
    private String APeriod;

    public StatisticsGenerator(List<Double> valueSequence) {
        this.valueSequence = valueSequence;
    }

    public double getExpectation() {
        double t = 0;
        for (double i : valueSequence) {
            t += i;
        }
        mx = t / valueSequence.size();
        return mx;
    }

    public double getDispersion() {
        double t = 0;
        for (double i : valueSequence) {
            t += Math.pow((i - mx), 2);
        }
        Dx = t / valueSequence.size();
        return Dx;
    }

    public double getMeanSquareDeviation() {
        sigmax = Math.sqrt(Dx);
        return sigmax;
    }

    public float[] getDistr() {
        float[] distrib = new float[20];
        List<Double> rstemp = new ArrayList<>(valueSequence);
        Collections.sort(rstemp);
        int c = 0;
        int index = 0;
        beg = rstemp.get(0);
        end = rstemp.get(rstemp.size() - 1);
        interv = (end - beg) / 20;
        for (int i = 0; i <= 19; i++) {
            while ((index <= rstemp.size() - 1) && (rstemp.get(index) <= beg + interv * (i +
                    1))) {
                if (rstemp.get(index) >= beg) c++;
                index++;
            }
            distrib[i] = (float) c / valueSequence.size();
            c = 0;
        }
        return distrib;
    }

    public int getPeriod() {
        period = valueSequence.size() - valueSequence.indexOf(valueSequence.size() - 1) - 1;
        return period;
    }

    public int getAPeriod() {
        int i = 0;
        while (valueSequence.get(i).equals(valueSequence.get(i + period-1))) {
            i++;
        }
        return i + period;//APeriod
    }

    public void ShowResults() {
       /* try {
            sw = new StreamWriter(pathname);
            foreach( double tmp in randomSequence)
            {
                sw.WriteLine(Convert.ToString(tmp));
            }
            sw.Close();
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo.FileName = pathname;
            p.Start();
        } catch
        {
            System.Windows.Forms.MessageBox.Show("Close Excel!!!");
        }*/
    }

    public double getChecked() {
        int t = 0;
        for (int i = 0; i <= valueSequence.size() - 3; i++) {
            if (Math.pow(valueSequence.get(i), 2) + Math.pow(valueSequence.get(i + 1), 2) <= 1) {
                t++;
            }
        }
        return (double) t / valueSequence.size();
    }

    public double getBeg() {
        return beg;
    }

    public void setBeg(double beg) {
        this.beg = beg;
    }

    public double getEnd() {
        return end;
    }

    public void setEnd(double end) {
        this.end = end;
    }

    public double getInterv() {
        return interv;
    }

    public void setInterv(double interv) {
        this.interv = interv;
    }

}
