package siammod;

import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import siammod.service.LehmerProducer;
import siammod.service.SequenceGenerator;
import siammod.service.StatisticsGenerator;
import siammod.service.distgen.*;

import java.math.BigDecimal;
import java.util.List;

public class Controller {

    public TextField inputA;
    public TextField inputM;
    public TextField inputR;
    public Label labelMxResult;
    public Label labelDxResult;
    public Label labelSdResult;
    public Label labelPeriodResult;
    public Label labelCheckResult;
    public Label labelAPeriodResult;
    public BarChart chart;
    public Tab triangularTab;
    public Tab simpsonsTab;
    public Tab gammaTab;
    public Tab exponentialTab;
    public Tab gaussTab;
    public Tab uniformTab;
    public Tab lemherTab;
    public TextField uniformA;
    public TextField uniformB;
    public TextField gaussM;
    public Label gaussSigma;
    public TextField exponentLambda;
    public TextField gammaNu;
    public TextField gammaLambda;
    public TextField triangularA;
    public TextField triangularB;
    public TextField simpsonsA;
    public TextField simpsonsB;

    public void performCalculation() {
        int a = Integer.parseInt(inputA.getText());
        int m = Integer.parseInt(inputM.getText());
        int r = Integer.parseInt(inputR.getText());
        SequenceGenerator sg = new LehmerProducer(a, m, r);
        if (triangularTab.isSelected()) {
            sg = new TriangularGenerator(a, m, r,
                    Double.parseDouble(triangularA.getText()),
                    Double.parseDouble(triangularB.getText()));
        }
        if (simpsonsTab.isSelected()) {
            sg = new SimpsonGenerator(a, m, r,
                    Double.parseDouble(simpsonsA.getText()),
                    Double.parseDouble(simpsonsB.getText()));
        }
        if (gammaTab.isSelected()) {
            sg = new GammaGenerator(a, m, r,
                    Double.parseDouble(gammaNu.getText()),
                    Double.parseDouble(gammaLambda.getText()));
        }
        if (exponentialTab.isSelected()) {
            sg = new ExponentialGenerator(a, m, r,
                    Double.parseDouble(exponentLambda.getText()));
        }
        if (gaussTab.isSelected()) {
            sg = new GaussGenerator(a, m, r,
                    Double.parseDouble(gaussM.getText()),
                    Double.parseDouble(gaussSigma.getText()));
        }
        if (uniformTab.isSelected()) {
            sg = new UniformGenerator(a, m, r,
                    Double.parseDouble(uniformA.getText()),
                    Double.parseDouble(uniformB.getText()));
        }
        List<Double> calculationResult = sg.provideSequence();
        StatisticsGenerator statisticsGenerator = new StatisticsGenerator(calculationResult);
        labelMxResult.setText("Mx: " + statisticsGenerator.getExpectation());
        labelDxResult.setText("Dx: " + statisticsGenerator.getDispersion());
        labelSdResult.setText("Sd: " + statisticsGenerator.getMeanSquareDeviation());
        labelPeriodResult.setText("Period: " + statisticsGenerator.getPeriod());
        labelAPeriodResult.setText("APeriod: " + statisticsGenerator.getAPeriod());
        labelCheckResult.setText("Check: " + statisticsGenerator.getChecked());
        XYChart.Series series = new XYChart.Series();
        series.setName("value");
        float[] distr = statisticsGenerator.getDistr();
        for (int i = 0; i < 19; i++) {
            float val = roundUp(statisticsGenerator.getBeg() + statisticsGenerator.getInterv() *
                    (i + 1), 3);
            System.out.println(val);
            series.getData().add(new XYChart.Data(Float.toString(val), distr[i]));
        }
        chart.getData().clear();
        chart.getData().add(series);
    }

    private float roundUp(double value, int digits) {
        BigDecimal result = new BigDecimal("" + value).setScale(digits, BigDecimal.ROUND_HALF_UP);
        float fresult = result.floatValue();
        return fresult;
    }
}
